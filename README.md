# ics-ans-post-install

Ansible playbook to run post installation tasks to configure a machine.

- configure the repository to point to artifactory
- install node-exporter for monitoring
- configure ntp
- add sssd on lab machines to allow the owner to access it
- drop facts about the current run of this playbook

Note that ntp is not configured on servers with the **esss.lu.se** domain
as they are managed by IT and already configured
(except machines on nat2-esss-lu-se network that we own).

The playbook will create inside the ansible fact.d directory a custom fact to register what domain was used when this playbook was run the last time.
The following custom fact are available:
- device_type
- ioc
- domain

Example on how to use the facts:
```
- name: Do something if the machine was moved from one domain to another
  debug:
    msg: "I'm not in the right domain."
  when: ansible_local.postinstall.domain != ansible_domain
```

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## License

BSD 2-clause
