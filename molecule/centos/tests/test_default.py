"""Tests for centos."""
import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_sssd_installed(host):
    """Test."""
    service = host.service("sssd")
    hostname = host.ansible.get_variables()["inventory_hostname"]
    # sssd installed on all lab VMs
    if hostname.endswith(".cslab.esss.lu.se"):
        assert service.is_running
        assert service.is_enabled
    else:
        assert not service.is_running


def test_chrony_installed(host):
    """Test."""
    service = host.service("chronyd")
    if host.ansible.get_variables()["inventory_hostname"].endswith("-gpn.esss.lu.se"):
        assert not service.is_running
    else:
        assert service.is_running
        assert service.is_enabled


def test_node_exporter_running(host):
    """Test."""
    service = host.service("node_exporter")
    assert service.is_enabled
    assert service.is_running
